﻿using ApiFurfooz.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Repositories
{
    public class TarifRepository : BaseRepository<Tarif>
    {
        public TarifRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
