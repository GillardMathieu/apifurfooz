﻿using ApiFurfooz.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Repositories
{
    public class InfoFurfoozRepository : BaseRepository<InfoFurfooz>
    {
        public InfoFurfoozRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
