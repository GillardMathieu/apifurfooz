﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Repositories
{
    public abstract class BaseRepository<T>
        where T : class, new()
    {
        private readonly string _connectionString;
        private readonly string _providerName;

        protected BaseRepository(string connectionString, string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
        }


        /// <summary>
        /// Retourne la liste des lignes à partir de "offset"
        /// Jusqu'à offset + limit
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>IEnumerable<T></returns>
        public virtual IEnumerable<T> Get()
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [" + typeof(T).Name
                    + "]";
                IDbCommand cmd = CreateCommand(conn, query, null);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }

        /// <summary>
        /// Retourne une instance de la ligne ciblée par l'id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T</returns>
        public virtual T GetById(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [" + typeof(T).Name + "] WHERE Id = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", id }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                if (r.Read())
                {
                    return ReaderToEntityMapper(r);
                }
                return null;
            }
        }

        /// <summary>
        /// Insère une ligne dans la table et retourne l'Id de la ligne insérée
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Insert(T entity)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                string query
                    = "INSERT INTO ["
                    + typeof(T).Name + "] (";
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += $"[{p.Name}],";
                }
                query = query.Substring(0, query.Length - 1);
                query += ") OUTPUT INSERTED.Id VALUES(";

                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id" && p.Name != "Image")
                        query += $"@{p.Name},";
                    if (p.Name == "Image")
                        query += $"CONVERT(VARBINARY(MAX),@{p.Name}),";
                }
                query = query.Substring(0, query.Length - 1);
                query += ")";
                Dictionary<string, object> parameters
                    = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                }
                IDbCommand cmd = CreateCommand(conn, query, parameters);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        /// <summary>
        /// Met à jour toutes les colonnes de la table
        /// Si elles correspondent aux propriétés de l'entité
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>bool</returns>
        public virtual bool Update(T entity)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "UPDATE [" + typeof(T).Name + "] SET ";
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id" && p.Name != "Image")
                        query += $"[{p.Name}] = @{p.Name},";
                    if (p.Name == "Image")
                        query += $"[{p.Name}] = CONVERT(VARBINARY(MAX),@{p.Name}),";
                }
                query = query.Substring(0, query.Length - 1);
                query += " WHERE Id = @Id";
                Dictionary<string, object> parameters
                    = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {
                    parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                }

                IDbCommand cmd = CreateCommand(conn, query, parameters);
                int nb = cmd.ExecuteNonQuery();
                return nb != 0;
            }
        }
        /// <summary>
        /// Supprime la ligne correspond à l'id
        /// et retourne vrai si la ligne a été supprimée
        /// Fonctionne si la colonne de la primary key s'appelle Id,
        /// si la colonne est un int
        /// et si la table à le même nom que l'entité
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public virtual bool Delete(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "DELETE FROM [" + typeof(T).Name + "] WHERE Id = @p1";
                Dictionary<string, object> parameters =
                    new Dictionary<string, object>()
                    {
                        { "@p1", id }
                    };
                IDbCommand cmd = CreateCommand(conn, query, parameters);
                int nbLines = cmd.ExecuteNonQuery();
                return nbLines != 0;
            }
        }

        /// <summary>
        /// Récupère la connection du répository
        /// </summary>
        /// <returns>IDbConnection</returns>
        protected IDbConnection GetConnection()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

            // crée ma connection
            IDbConnection connection = factory.CreateConnection();

            // set ma connection à ma connection
            connection.ConnectionString = _connectionString;

            return connection;
        }

        /// <summary>
        /// Crée une commande Sql à partir d'une connection, d'une query SQL et d'une dictionnaire de paramètres
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="text"></param>
        /// <param name="parameters"></param>
        /// <returns>IDbCommand</returns>
        protected IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        {
            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = text;
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> kvp in parameters)
                {
                    IDataParameter p = cmd.CreateParameter();
                    p.ParameterName = kvp.Key;
                    p.Value = kvp.Value;
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
        }

        /// <summary>
        /// Créer une Instance de T à partir d'un IDataReader
        /// Ne Fonctionne que si les colonnes de l'entité correspondent aux colonnes de la db
        /// </summary>
        /// <param name="r"></param>
        /// <returns>T</returns>
        protected T ReaderToEntityMapper(IDataReader r)
        {
            T s = new T();
            IEnumerable<PropertyInfo> properties = s.GetType().GetProperties();
            foreach (PropertyInfo p in properties)
            {
                p.SetValue(s, r[p.Name] == DBNull.Value ? null : r[p.Name]);
            }
            return s;
        }
    }
}
