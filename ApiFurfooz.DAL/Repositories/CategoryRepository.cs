﻿using ApiFurfooz.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Repositories
{
    public class CategoryRepository : BaseRepository<Category>
    {
        public CategoryRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
