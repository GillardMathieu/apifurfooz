﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Entities
{
    public class Tarif
    {
        public int Age { get; set; }
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}
