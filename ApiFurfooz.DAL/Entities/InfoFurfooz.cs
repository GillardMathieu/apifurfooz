﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Entities
{
    public class InfoFurfooz
    {
        public MailAddress Email { get; set; }
        public string Tel { get; set; }
        public string ConservatorName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
