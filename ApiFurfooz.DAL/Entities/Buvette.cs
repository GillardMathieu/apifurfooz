﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiFurfooz.DAL.Entities
{
    public class Buvette
    {
        public int Id { get; set; }
        public string Name_fr { get; set; }
        public string Name_en { get; set; }
        public string Name_nl { get; set; }
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
        public int Category_Id { get; set; }
    }
}
