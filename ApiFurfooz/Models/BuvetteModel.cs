﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class BuvetteModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        [MinLength(2)]
        public string Name_fr { get; set; }

        [MaxLength(256)]
        [MinLength(2)]
        public string Name_en { get; set; }

        [MaxLength(256)]
        [MinLength(2)]
        public string Name_nl { get; set; }

        [Required]
        [Range(0,999)]
        public double Price { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        public int Category_Id { get; set; }

    }
}