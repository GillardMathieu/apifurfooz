﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ApiFurfooz.Models
{
    public class InfoFurfoozModel
    {

        [Required]
        public MailAddress Email { get; set; }

        [Required]
        public string Tel { get; set; }

        [Required]
        [MaxLength(256)]
        [MinLength(2)]
        public string ConservatorName { get; set; }
        public bool IsDeleted { get; set; }
    }
}