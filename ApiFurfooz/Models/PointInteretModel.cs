﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class PointInteretModel
    {
		public int Id { get; set; }

		[Required]
		[MaxLength(256)]
		[MinLength(2)]
		public string Name_fr { get; set; }

		[MaxLength(256)]
		[MinLength(2)]
		public string Name_nl { get; set; }

		[MaxLength(256)]
		[MinLength(2)]
		public string Name_en { get; set; }
		public byte[] Image { get; set; }

		[MaxLength(256)]
		[MinLength(2)]
		public string Description_fr { get; set; }

		[MaxLength(256)]
		[MinLength(2)]
		public string Description_en { get; set; }

		[MaxLength(256)]
		[MinLength(2)]
		public string Description_nl { get; set; }


		[Range(-90,90)]
		public double? Latitude { get; set; }

		[Range(-180,180)]
		public double? Longitude { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public double? Interval { get; set; }
		public bool IsDeleted { get; set; }
		public int? Category_id { get; set; }
		//public string MimeType { get; set; }
	}
}