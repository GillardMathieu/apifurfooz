﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class CategoryProductModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        [MinLength(2)]
        public string Name_fr { get; set; }

        [MaxLength(256)]
        [MinLength(2)]
        public string Name_nl { get; set; }

        [MaxLength(256)]
        [MinLength(2)]
        public string Name_en { get; set; }
        public bool IsDeleted { get; set; }

    }
}