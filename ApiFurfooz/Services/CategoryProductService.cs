﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class CategoryProductService
    {
        private CategoryProductRepository repo;
        public CategoryProductService(CategoryProductRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<CategoryProductModel> GetAll()
        {
            return repo.Get().Select(x => x.MapTo<CategoryProductModel>());
        }

        public CategoryProduct GetById(int id)
        {
            return repo.GetById(id);
        }

        public int Insert(CategoryProduct a)
        {
            return repo.Insert(a);
        }

        public void Update(CategoryProduct a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }
    }
}