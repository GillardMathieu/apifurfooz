﻿using ApiFurfooz.Controllers;
using ApiFurfooz.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace ApiFurfooz.Services
{
    public class ServiceLocator: IDependencyResolver
    {
        private  ServiceCollection services = new ServiceCollection();
        private  ServiceProvider Provider;

        public ServiceLocator()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider =
                ConfigurationManager.ConnectionStrings["default"].ProviderName;
            services.AddScoped<BuvetteRepository>(
                (x) => new BuvetteRepository(connectionString, provider)
                );
            services.AddScoped<CategoryProductRepository>(
                (x) => new CategoryProductRepository(connectionString, provider)
                );
            services.AddScoped(
                (x) => new DAL.Repositories.CategoryRepository(connectionString, provider)
                 );
            services.AddScoped<InfoFurfoozRepository>(
                (x) => new InfoFurfoozRepository(connectionString, provider)
                 );
            services.AddScoped<PointInteretRepository>(
                (x) => new PointInteretRepository(connectionString, provider)
                 );
            services.AddScoped<TarifRepository>(
                (x) => new TarifRepository(connectionString, provider)
                 );
            services.AddScoped<CategoryRepository>(
                (x) => new CategoryRepository
                (connectionString, provider)
                 );

            services.AddScoped<BuvetteService>();
            services.AddScoped<CategoryProductService>();
            services.AddScoped<CategoryService>();
            services.AddScoped<InfoFurfoozService>();
            services.AddScoped<PointInteretService>();
            services.AddScoped<TarifService>();


            services.AddScoped<PointInteretController>();
            services.AddScoped<BuvetteController>();
            services.AddScoped<CategoryController>();
            services.AddScoped<CategoryProductController>();
            services.AddScoped<InfoFurfoozController>();
            services.AddScoped<TarifController>();

            Provider = services.BuildServiceProvider();
        }
        public  T GetService<T>()
        {
            return Provider.GetService<T>();
        }

        public IDependencyScope BeginScope()
        {
            return new ServiceLocator();
        }

        private bool disposed = false;

        private void Dispose(bool disposing)
        {
            if (disposed) return;

            if(disposing)
            {
                Provider.Dispose();
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public object GetService(Type serviceType)
        {
            return Provider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Provider.GetServices(serviceType);
        }
    }
}