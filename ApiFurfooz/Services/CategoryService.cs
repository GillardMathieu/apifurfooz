﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class CategoryService
    {
        private CategoryRepository repo;
        public CategoryService(CategoryRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<CategoryModel> GetAll()
        {
            return repo.Get().Select(x => x.MapTo<CategoryModel>());
        }

        public Category GetById(int id)
        {
            return repo.GetById(id);
        }

        public int Insert(Category a)
        {
            return repo.Insert(a);
        }

        public void Update(Category a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }

    }
}