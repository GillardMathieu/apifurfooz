﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class InfoFurfoozService
    {
        private InfoFurfoozRepository repo;
        public InfoFurfoozService(InfoFurfoozRepository repo)
        {
            this.repo = repo;
        }


        public IEnumerable<InfoFurfoozModel> GetAll()
        {
            return repo.Get().Select(x => x.MapTo<InfoFurfoozModel>());
        }

        public InfoFurfooz GetById(int id)
        {
            return repo.GetById(id);
        }

        public int Insert(InfoFurfooz a)
        {
            return repo.Insert(a);
        }

        public void Update(InfoFurfooz a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }

    }
}