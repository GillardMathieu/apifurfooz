﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace ApiFurfooz.Services
{
    public class BuvetteService
    {
        private BuvetteRepository repo;
        public BuvetteService(BuvetteRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<BuvetteModel> GetAll()
        {
            return repo.Get().Select(x=>x.MapTo<BuvetteModel>());
        }

        public Buvette GetById(int id)
        {
            return repo.GetById(id);
        }

        public int Insert(Buvette a)
        {
            return repo.Insert(a);
        }

        public void Update(Buvette a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }

    }
}