﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class PointInteretService
    {

        private PointInteretRepository repo;
        public PointInteretService(PointInteretRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<PointInteretModel> GetAll()
        {
            return repo.Get().Select(x => x.MapTo<PointInteretModel>());
        }

        public PointInteret GetById(int id)
        {
            PointInteret pi = repo.GetById(id);
            return pi;
        }

        public int Insert(PointInteret a)
        {
            return repo.Insert(a);
        }

        public void Update(PointInteret a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }
    }
}