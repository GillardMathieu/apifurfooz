﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class TarifService
    {
        private TarifRepository repo;
        public TarifService(TarifRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<TarifModel> GetAll()
        {
            return repo.Get().Select(x => x.MapTo<TarifModel>());
        }

        public Tarif GetById(int id)
        {
            return repo.GetById(id);
        }

        public int Insert(Tarif a)
        {
            return repo.Insert(a);
        }

        public void Update(Tarif a)
        {
            repo.Update(a);
        }

        public void Delete(int id)
        {
            repo.Delete(id);
        }
    }
}