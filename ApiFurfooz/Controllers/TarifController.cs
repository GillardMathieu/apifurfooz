﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class TarifController : ApiController
    {

        private TarifService service;
        public TarifController(TarifService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<TarifModel> Get()
        {
            IEnumerable<TarifModel> tarifs = service.GetAll();
            return tarifs;
        }

        [HttpGet]

        public Tarif GetById(int id)
        {

            var tarif = service.GetById(id);
            if (tarif == null)
            {
                return null;
            }
            else
            {
                return tarif;
            }
        }


        [HttpPut]
        public void Update(Tarif a)
        {
            service.Update(a);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            service.Delete(id);
        }


        [HttpPost]
        public int Insert(Tarif a)
        {
            return service.Insert(a);
        }
    }
}