﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class CategoryController : ApiController
    {

        private CategoryService service;

        public CategoryController(CategoryService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<CategoryModel> Get()
        {
            IEnumerable<CategoryModel> categories = service.GetAll();
            return categories;
        }

        [HttpGet]

        public Category GetById(int id)
        {
            var category = service.GetById(id);
            if (category == null)
            {
                return null;
            }
            else
            {
                return category;
            }
        }


        [HttpPut]
        public void Update(Category a)
        {
            service.Update(a);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            service.Delete(id);
        }


        [HttpPost]
        public int Insert(Category a)
        {
            return service.Insert(a);
        }
    }
}