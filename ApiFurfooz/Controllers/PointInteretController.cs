﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Mvc.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace ApiFurfooz.Controllers
{
    public class PointInteretController : ApiController
    {

        private PointInteretService service;
        public PointInteretController(PointInteretService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<PointInteretModel> Get()
        {
            IEnumerable<PointInteretModel> PointsOfInterest = service.GetAll();
            return PointsOfInterest;
        }

        [HttpGet]

        public PointInteret GetById(int id)
        {

            var pi = service.GetById(id);
            if (pi == null)
            {
                return null;
            }
            else
            {
                return pi;
            }
        }


        [HttpPut]
        public void Update(PointInteret p)
        {
            service.Update(p);
        }


        [HttpDelete]
        public void Delete(int id)
        {
            service.Delete(id);
        }


        [HttpPost]
        public string Insert(PointInteret p)
        {
            try
            {
                return service.Insert(p).ToString();

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}