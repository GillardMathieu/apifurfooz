﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ApiFurfooz.Controllers
{
    public class CategoryProductController
    {

        private CategoryProductService service;

        public CategoryProductController(CategoryProductService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<CategoryProductModel> Get()
        {
            IEnumerable<CategoryProductModel> Categories = service.GetAll();
            return Categories;
        }


        [HttpGet]

        public CategoryProduct GetById(int id)
        {
            var categoryPro = service.GetById(id);
            if (categoryPro == null)
            {
                return null;
            }
            else
            {
                return categoryPro;
            }
        }


        [HttpPut]

        public void Update(CategoryProduct a)
        {
            service.Update(a);
        }



        [HttpDelete]

        public void Delete(int id)
        {
            service.Delete(id);
        }


        [HttpPost]

        public int Insert(CategoryProduct a)
        {
           return service.Insert(a);
        }

    }
}