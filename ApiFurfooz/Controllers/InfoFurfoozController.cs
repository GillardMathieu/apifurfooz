﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class InfoFurfoozController : ApiController
    {

        private InfoFurfoozService service;
        public InfoFurfoozController(InfoFurfoozService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<InfoFurfoozModel> Get()
        {
            IEnumerable<InfoFurfoozModel> infos = service.GetAll();
            return infos;
        }

        [HttpGet]

        public InfoFurfooz GetById(int id)
        {

            var info = service.GetById(id);
            if (info == null)
            {
                return null;
            }
            else
            {
                return info;
            }
        }


        [HttpPut]
        public void Update(InfoFurfooz a)
        {
            service.Update(a);
        }



        [HttpDelete]
        public void Delete(int id)
        {
            service.Delete(id);
        }


        [HttpPost]
        public int Insert(InfoFurfooz a)
        {
            return service.Insert(a);
        }
    }
}