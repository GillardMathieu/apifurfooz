﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class BuvetteController : ApiController
    {
        private BuvetteService service;

        public BuvetteController(BuvetteService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<BuvetteModel> Get()
        {
            IEnumerable<BuvetteModel> Buvettes = service.GetAll();
            return Buvettes;
            
        }

        [HttpGet]

        public Buvette GetById(int id)
        {
            var buvette = service.GetById(id);
            if(buvette == null)
            {
                return null;
            }
            else
            {
                return buvette;
            }
        }


        [HttpPut]
        public void Update (Buvette a)
        {
            service.Update(a);
        }


        [HttpDelete]
        public void Delete (int id)
        {
            service.Delete(id);
        }



        [HttpPost]
        public int Insert (Buvette a)
        {
            return service.Insert(a);
        }

    }
}